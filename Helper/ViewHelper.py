from django.shortcuts import render_to_response


def handler400(request):
    """	Bad request
    """
    return render_to_response("common/400.html")


def handler403(request):
    """	Permission denied
    """
    return render_to_response("common/403.html")


def handler404(request):
    """	Not Found
    """
    return render_to_response("common/404.html")


def handler500(request):
    """	Server Error
    """
    return render_to_response("common/500.html")
