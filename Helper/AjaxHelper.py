from django.http import JsonResponse


def ajax_ok(data, message):
    return JsonResponse(
                {
                    "error": 0,
                    "message": message,
                    "data": data
                },
                safe=False
    )


def ajax_bad_request(message):
    return JsonResponse(
                {
                    "error": 1,
                    "message": message,
                    "data": {}
                },
                status=400
    )


def ajax_error(message):
    return JsonResponse(
                {
                    "error": 1,
                    "message": message,
                    "data": {}
                },
                status=200
    )
