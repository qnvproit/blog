from django import template
import datetime

register = template.Library()


@register.filter(name="converttimestamp")
def timestamp_to_human(timestamp, arg="%Y-%m-%d %H:%M:%S"):
    """ Convert timestamp to humanreadable,
            timestamp measure by 'second'
    """
    return datetime.datetime.fromtimestamp(int(timestamp)).strftime(arg)


@register.filter(name="avatar")
def get_full_url_avatar(avatar):
    return (ConstPost.PREFIX_AVATAR_POST + 'noavatar.png') if avatar is None else (ConstPost.PREFIX_AVATAR_POST + avatar)
