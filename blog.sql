-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 15, 2015 at 02:44 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE IF NOT EXISTS `auth_group` (
`id` int(11) NOT NULL,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
`id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE IF NOT EXISTS `auth_permission` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can add permission', 2, 'add_permission'),
(5, 'Can change permission', 2, 'change_permission'),
(6, 'Can delete permission', 2, 'delete_permission'),
(7, 'Can add group', 3, 'add_group'),
(8, 'Can change group', 3, 'change_group'),
(9, 'Can delete group', 3, 'delete_group'),
(10, 'Can add user', 4, 'add_user'),
(11, 'Can change user', 4, 'change_user'),
(12, 'Can delete user', 4, 'delete_user'),
(13, 'Can add content type', 5, 'add_contenttype'),
(14, 'Can change content type', 5, 'change_contenttype'),
(15, 'Can delete content type', 5, 'delete_contenttype'),
(16, 'Can add session', 6, 'add_session'),
(17, 'Can change session', 6, 'change_session'),
(18, 'Can delete session', 6, 'delete_session'),
(19, 'Can add type', 7, 'add_type'),
(20, 'Can change type', 7, 'change_type'),
(21, 'Can delete type', 7, 'delete_type'),
(22, 'Can add post', 8, 'add_post'),
(23, 'Can change post', 8, 'change_post'),
(24, 'Can delete post', 8, 'delete_post'),
(25, 'Can add category', 9, 'add_category'),
(26, 'Can change category', 9, 'change_category'),
(27, 'Can delete category', 9, 'delete_category'),
(28, 'Can add post category', 10, 'add_postcategory'),
(29, 'Can change post category', 10, 'change_postcategory'),
(30, 'Can delete post category', 10, 'delete_postcategory'),
(31, 'Can add comment', 11, 'add_comment'),
(32, 'Can change comment', 11, 'change_comment'),
(33, 'Can delete comment', 11, 'delete_comment');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE IF NOT EXISTS `auth_user` (
`id` int(11) NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE IF NOT EXISTS `auth_user_groups` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE IF NOT EXISTS `django_admin_log` (
`id` int(11) NOT NULL,
  `action_time` datetime NOT NULL,
  `object_id` longtext COLLATE utf8_unicode_ci,
  `object_repr` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE IF NOT EXISTS `django_content_type` (
`id` int(11) NOT NULL,
  `app_label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(5, 'contenttypes', 'contenttype'),
(9, 'PostApp', 'category'),
(11, 'PostApp', 'comment'),
(8, 'PostApp', 'post'),
(10, 'PostApp', 'postcategory'),
(7, 'PostApp', 'type'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE IF NOT EXISTS `django_migrations` (
`id` int(11) NOT NULL,
  `app` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `applied` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2015-07-09 13:07:27'),
(2, 'auth', '0001_initial', '2015-07-09 13:07:34'),
(3, 'admin', '0001_initial', '2015-07-09 13:07:36'),
(4, 'contenttypes', '0002_remove_content_type_name', '2015-07-09 13:07:37'),
(5, 'auth', '0002_alter_permission_name_max_length', '2015-07-09 13:07:37'),
(6, 'auth', '0003_alter_user_email_max_length', '2015-07-09 13:07:38'),
(7, 'auth', '0004_alter_user_username_opts', '2015-07-09 13:07:38'),
(8, 'auth', '0005_alter_user_last_login_null', '2015-07-09 13:07:39'),
(9, 'auth', '0006_require_contenttypes_0002', '2015-07-09 13:07:39'),
(10, 'sessions', '0001_initial', '2015-07-09 13:07:39'),
(11, 'PostApp', '0001_initial', '2015-09-09 15:53:55');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `session_data` longtext COLLATE utf8_unicode_ci NOT NULL,
  `expire_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE IF NOT EXISTS `tbl_category` (
`id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `title`) VALUES
(1, 'Python'),
(2, 'PHP'),
(3, 'Tools'),
(4, 'Javascript'),
(5, 'AngularJS'),
(6, 'HTML, CSS'),
(7, 'Linux'),
(8, 'Backend'),
(9, 'Frontend');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category_post`
--

CREATE TABLE IF NOT EXISTS `tbl_category_post` (
`id` int(11) NOT NULL,
  `id_post` int(10) NOT NULL,
  `id_category` int(10) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_category_post`
--

INSERT INTO `tbl_category_post` (`id`, `id_post`, `id_category`) VALUES
(5, 1, 1),
(7, 3, 1),
(8, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comment`
--

CREATE TABLE IF NOT EXISTS `tbl_comment` (
`id` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `id_post` int(10) DEFAULT NULL,
  `id_parent_cmt` int(11) NOT NULL,
  `created_time` bigint(20) NOT NULL,
  `name_owner` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `email_owner` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  `is_notifications` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=35 ;

--
-- Dumping data for table `tbl_comment`
--

INSERT INTO `tbl_comment` (`id`, `content`, `id_post`, `id_parent_cmt`, `created_time`, `name_owner`, `email_owner`, `active`, `is_notifications`) VALUES
(1, 'Tạo shortcode sử dụng tham số Ở phần trước chúng ta chỉ tìm hiểu qua cách tạo một shortcode đơn giản, nghĩa là nó chỉ hiển thị y chóc những gì ta đã viết vào shortcode mà không cho phép chỉnh lại theo ý muốn của người sử dụng. Nếu bạn muốn cho người dùng có thể tự sửa lại những gì hiển thị ra thì ở đây chúng ta phải sử dụng các tham số. Ví dụ, ở đoạn shortcode trên chúng ta đã cho hiển thị 10 bài viết ngẫu nhiên. Nhưng nếu khi sử dụng tham số, chúng ta có thể cho người dùng tùy biến lại tham số ở phần số lượng bài viết hiển thị ra và có thể tùy chọn thứ tự sắp xếp nếu bạn muốn. Để tạo shortcode có chứa tham số, ta tạo lại shortcode như sau:', 1, 0, 1234234234234, 'Hoang Nguyen', 'hoangnguyen@gmail.com', 1, 0),
(2, 'Tạo shortcode sử dụng tham số Ở phần trước chúng ta chỉ tìm hiểu qua cách tạo một shortcode đơn giản, nghĩa là nó chỉ hiển thị y chóc những gì ta đã viết vào shortcode mà không cho phép chỉnh lại theo ý muốn của người sử dụng. Nếu bạn muốn cho người dùng có thể tự sửa lại những gì hiển thị ra thì ở đây chúng ta phải sử dụng các tham số. Ví dụ, ở đoạn shortcode trên chúng ta đã cho hiển thị 10 bài viết ngẫu nhiên. Nhưng nếu khi sử dụng tham số, chúng ta có thể cho người dùng tùy biến lại tham số ở phần số lượng bài viết hiển thị ra và có thể tùy chọn thứ tự sắp xếp nếu bạn muốn. Để tạo shortcode có chứa tham số, ta tạo lại shortcode như sau:Tạo shortcode sử dụng tham số Ở phần trước chúng ta chỉ tìm hiểu qua cách tạo một shortcode đơn giản, nghĩa là nó chỉ hiển thị y chóc những gì ta đã viết vào shortcode mà không cho phép chỉnh lại theo ý muốn của người sử dụng. Nếu bạn muốn cho người dùng có thể tự sửa lại những gì hiển thị ra thì ở đây chúng ta phải sử dụng các tham số. Ví dụ, ở đoạn shortcode trên chúng ta đã cho hiển thị 10 bài viết ngẫu nhiên. Nhưng nếu khi sử dụng tham số, chúng ta có thể cho người dùng tùy biến lại tham số ở phần số lượng bài viết hiển thị ra và có thể tùy chọn thứ tự sắp xếp nếu bạn muốn. Để tạo shortcode có chứa tham số, ta tạo lại shortcode như sau:', 1, 0, 23411242354534, 'Tuan Nguyen', 'tuannguyen@gmail.com', 1, 0),
(3, 'ửetưetert ẻ tửet eử tẻ ', 1, 1, 124534534, 'teửt', 'erwt', 1, 0),
(4, 'fhjkdsàhjk fadjshfjákd klfadhfkjasdh kjàshd fkjsadjksadfdf ', 1, 1, 124534534, 'Quy Nguyen', 'quynguyen', 1, 1),
(5, 'fhjkdsàhjk fadjshfjákd klfadhfkjasdh kjàshd fkjsadjksadfdf ', 1, 2, 1440842330, 'Quy Nguyen', 'quynguyen', 1, 1),
(6, 'nguyen vu quy dai hoc bach khoa ha noi', 1, 0, 1440842485, 'Hoang Anh', 'hoanganh@gmail.com', 1, 0),
(7, 'nguyen vu quy dai hoc bach khoa ha noi', 1, 0, 1440842535, 'Hoang Anh', 'hoanganh@gmail.com', 1, 0),
(8, 'nguyen vu quy dai hoc bach khoa ha noi', 1, 0, 1440842963, 'Hoang Anh', 'hoanganh@gmail.com', 1, 0),
(9, 'nguyen vu quy dai hoc bach khoa ha noi', 1, 0, 1440842996, 'Hoang Anh', 'hoanganh@gmail.com', 1, 0),
(10, 'jàhkh kàjsdhkj ájdhfjk jkáhf', 1, 0, 1440888937, 'quy nguyen', 'quynguyen', 1, 0),
(11, 'jàhkh kàjsdhkj ájdhfjk jkáhf', 1, 0, 1440888989, 'quy nguyen', 'quynguyen', 1, 0),
(12, 'jàhkh kàjsdhkj ájdhfjk jkáhf', 1, 0, 1440889001, 'quy nguyen', 'quynguyen', 1, 0),
(13, 'jàhkh kàjsdhkj ájdhfjk jkáhf', 1, 0, 1440889073, 'quy nguyen', 'quynguyen', 1, 0),
(14, 'jàhkh kàjsdhkj ájdhfjk jkáhf', 1, 0, 1440889114, 'quy nguyen', 'quynguyen', 1, 0),
(15, 'jàhkh kàjsdhkj ájdhfjk jkáhf', 1, 0, 1440889142, 'quy nguyen', 'quynguyen', 1, 0),
(16, 'jàhkh kàjsdhkj ájdhfjk jkáhf', 1, 0, 1440889151, 'quy nguyen', 'quynguyen', 1, 0),
(17, 'jàhkh kàjsdhkj ájdhfjk jkáhf', 1, 0, 1440889788, 'quy nguyen', 'quynguyen', 1, 0),
(18, 'jàhkh kàjsdhkj ájdhfjk jkáhf', 1, 0, 1440890477, 'quy nguyen', 'quynguyen', 1, 0),
(19, 'jàhkh kàjsdhkj ájdhfjk jkáhf', 1, 0, 1440891866, 'quy nguyen', 'quynguyen', 1, 0),
(20, 'jàhkh kàjsdhkj ájdhfjk jkáhf', 1, 0, 1440894767, 'quy nguyen', 'quynguyen', 1, 0),
(21, 'hello, iam a newbie. hi all', 1, 0, 1440982863, 'Hoang Gia Lam', 'nvqbkhn@gmail.com', 1, 1),
(22, 'trỷtỷe tryt ỷty rtỷt ryt yẻt ỷty ', 1, 0, 1440983598, 'ỷtỷtỷtyẻt', 'ỷteyẻtyẻty', 1, 0),
(23, 'xcvxcv cx vxcv xcv xc vcx v', 1, 0, 1440983663, 'vccxv', 'xcvxcvxc', 1, 0),
(24, 'ẻ tẻtẻ tẻ t tưẻtẻt ẻt ', 1, 0, 1440983820, 'tewrt', 'ewr tẻ t', 1, 0),
(25, 'gdf gd fdgf gdf gdf gdf dg', 1, 0, 1440983927, 'gdfgdf gdf', 'dfg df dgf', 1, 0),
(26, 'fsdấd fsdád fsd fsd fád f', 1, 0, 1440984067, 'quy vu', 'fhákdjfhkj fkjsadhfjk', 1, 0),
(27, 'dh fgh dfg hdf ghdfg hdf ghdf ghdfg ', 1, 0, 1440984098, 'dghfh gfh ', 'fg hdfg hfg hfg h', 1, 0),
(28, ' ẻ tẻ te tr trtwẻtwẻtewrtẻt ưẻ tẻ tư ẻte r', 1, 0, 1440984183, 'tewrtẻt', 'wẻ tẻtẻ tẻ', 1, 0),
(29, ' dsfgdfsg dsf', 1, 0, 1440984638, 'gdfsgsdfg', 'g dfg dfgdf gds gdf', 1, 0),
(30, 'ẻ gewr gưe gưẻ ge rgẻgưẻg', 1, 0, 1440984689, 'gưẻgưẻg ', 'we rgewrgưẻg wẻ', 1, 0),
(31, 'hỷtyhrthy rty hrty hty', 1, 0, 1440984798, 'rthrtyhrtyhrtyh', 'hrty h hrtỷ', 1, 0),
(32, ' rưqẻ ưqe rưe rưe  rqwe r', 1, 0, 1440984861, 'rqwểwr', 'weqrưqẻwqe', 1, 0),
(33, 'sad fád fád fád fá dfád ', 1, 0, 1440985001, 'gsdfgsdfg sdfg sdfg sdf gdsf', 'fsdfsadf ád fád f', 1, 0),
(34, 'hay day ! perfect blog :)))', 2, 0, 1441288949, 'Quy Nguyen', 'quynguyen@gmail.com', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post`
--

CREATE TABLE IF NOT EXISTS `tbl_post` (
`id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_time` bigint(20) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `type_post` int(11) DEFAULT NULL,
  `number_views` int(11) NOT NULL DEFAULT '0',
  `avatar` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_post`
--

INSERT INTO `tbl_post` (`id`, `title`, `content`, `created_time`, `active`, `type_post`, `number_views`, `avatar`) VALUES
(1, 'The test post have', '<b>Cách đây }} </b> &gt; hơn một năm mình đã có một tutorial cách tự viết một shortcode dành cho riêng mình. Nhưng sau khi đọc lại bài đó thì thấy khá lủng củng ở nhiều chi tiết và quá dài nên ở bài này mình xin viết lại theo hướng dễ hiểu nhất cho những ai chưa biết, cũng như tóm gọn lại những ý quan trọng cần truyền tải.  Shortcode là gì?  Shortcode dịch theo đúng tiếng Việt nghĩa là Code ngắn hay nói cách khác là một đoạn code ngắn. Đoạn code ngắn này sẽ thực thi những tác vụ gì đó mà bạn đã định sẵn trong lúc tạo shortcode, ví dụ như hiển thị một Loop chẳng hạn. Bạn có thể thực thi shortcode này ở bất cứ đâu như trong bài viết, trong theme, ngoại trừ excerpt và widget, nhưng mình sẽ có cách hướng dẫn bạn khắc phục.  Bây giờ shortcode được sử dụng khá phổ biến, bạn có thể vào thư viện plugin WordPress và tìm plugin với từ khóa shortcode ra là có rất nhiều plugin hỗ trợ bạn sẵn một số shortcode đủ yêu cầu, nào là trang trí bài viết, nào là làm các công việc phức tạp hơn. Và trong nhiều theme, nó cũng có thể hỗ trợ sẵn một vài shortcode của nó.  Cách tạo shortcode...Toàn bộ code trong bài này bạn viết vào file functions.php của theme nhé.  Để tạo một shortcode ta sẽ bao gồm 2 bước chính:  Thiết lập function thực thi code trong shortcode. Tạo một tên shortcode dựa vào function đã tạo cho nó. Để dễ hình dung, mình đưa ra một mẫu tạo shortcode thế này. Bạn có thể thấy từ đoạn 08 đến 21 mình viết loop bên trong hàm ob_start() và ob_end_clean(). Thực chất ở đây mục đích của mình không phải là cache gì cả mà mình sẽ wrap phần đó lại để mình dùng hàm ob_get_contents() vào biến $list_post, sau đó là return cái biến này ra mà thôi vì khi làm shortcode mình phải dùng return mà. Nếu ai có cách nào hay hơn thì cho mình biết nhé.  Bây giờ thì bạn viết shortcode [random_post] vào vị trí mà bạn cần hiển thị danh sách bài ngẫu nhiên trong bài là xong.  Tạo shortcode sử dụng tham số  Ở phần trước chúng ta chỉ tìm hiểu qua cách tạo một shortcode đơn giản, nghĩa là nó chỉ hiển thị y chóc những gì ta đã viết vào shortcode mà không cho phép chỉnh lại theo ý muốn của người sử dụng. Nếu bạn muốn cho người dùng có thể tự sửa lại những gì hiển thị ra thì ở đây chúng ta phải sử dụng các tham số.  Ví dụ, ở đoạn shortcode trên chúng ta đã cho hiển thị 10 bài viết ngẫu nhiên. Nhưng nếu khi sử dụng tham số, chúng ta có thể cho người dùng tùy biến lại tham số ở phần số lượng bài viết hiển thị ra và có thể tùy chọn thứ tự sắp xếp nếu bạn muốn.  Để tạo shortcode có chứa tham số, ta tạo lại shortcode như sau: &gt;', 1437013337, 1, 1, 110, 'python.png'),
(2, 'The second Post', 'We have provided detailed documentation as well as specific code examples to help new users get started. We are also always open to feedback and can answer any questions a user may have about Materialize.', 1437015000, 1, 2, 35, 'php.png'),
(3, 'Học PHP cho WordPress và các tài nguyên cần thiết', 'Nếu bạn đã yêu thích mã nguồn WordPress và muốn đi sâu hơn về nó, cụ thể là lập trình các tính năng trong WordPress, viết plugin hoặc làm theme thì bạn phải thành thạo rất nhiều kiến thức. Những kiến thức cần thiết để làm web thì mình không nói rồi nhé, nhưng một trong các kiến thức quan trọng nhất để có thể lập trình các tính năng trong mã nguồn WordPress là ngôn ngữ lập trình PHP. Bởi vì WordPress được viết bằng ngôn ngữ PHP để tương tác với cơ sở dữ liệu dùng hệ quản trị MySQL nên bạn muốn “giao tiếp” được với code trong WordPress thì phải biết PHP.\r\n\r\nThực ra mình đã viết xong một serie Học PHP dành cho nhu cầu làm việc trong WordPress, nhưng thật trớ trêu là đến khi viết xong thì đọc lại mình thấy là chuối quá và có khi là bạn sẽ càng khó hiểu PHP thêm. Nên mình quyết định là không đăng serie đó nhưng sẽ viết một bài guide là bài này để hướng dẫn bạn từng bước tìm hiểu PHP để có thể làm việc với WordPress.', 1437019000, 1, 4, 54, 'php.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subscriber`
--

CREATE TABLE IF NOT EXISTS `tbl_subscriber` (
`id` int(11) NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_subscriber`
--

INSERT INTO `tbl_subscriber` (`id`, `email`, `active`) VALUES
(1, 'qnvproit@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tags`
--

CREATE TABLE IF NOT EXISTS `tbl_tags` (
`id` int(11) NOT NULL,
  `tags` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tag_post`
--

CREATE TABLE IF NOT EXISTS `tbl_tag_post` (
`id` int(11) NOT NULL,
  `id_tag` int(11) NOT NULL,
  `id_post` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_type`
--

CREATE TABLE IF NOT EXISTS `tbl_type` (
`id` int(11) NOT NULL,
  `type_post` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_type`
--

INSERT INTO `tbl_type` (`id`, `type_post`, `active`) VALUES
(1, 'PHP', 1),
(2, 'Python', 1),
(3, 'HTML, CSS', 1),
(4, 'Javascript', 1),
(5, 'Tools', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `group_id` (`group_id`,`permission_id`), ADD KEY `auth_group__permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `content_type_id` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `user_id` (`user_id`,`group_id`), ADD KEY `auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `user_id` (`user_id`,`permission_id`), ADD KEY `auth_user_u_permission_id_384b62483d7071f0_fk_auth_permission_id` (`permission_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
 ADD PRIMARY KEY (`id`), ADD KEY `djang_content_type_id_697914295151027a_fk_django_content_type_id` (`content_type_id`), ADD KEY `django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `django_content_type_app_label_45f3b1d93ec8c61c_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
 ADD PRIMARY KEY (`session_key`), ADD KEY `django_session_de54fa62` (`expire_date`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tbl_category_post`
--
ALTER TABLE `tbl_category_post`
 ADD PRIMARY KEY (`id`), ADD KEY `id_post` (`id_post`), ADD KEY `id_category` (`id_category`);

--
-- Indexes for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
 ADD PRIMARY KEY (`id`), ADD KEY `id_post` (`id_post`);

--
-- Indexes for table `tbl_post`
--
ALTER TABLE `tbl_post`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`), ADD KEY `type_post` (`type_post`);

--
-- Indexes for table `tbl_subscriber`
--
ALTER TABLE `tbl_subscriber`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `tbl_tags`
--
ALTER TABLE `tbl_tags`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_tag_post`
--
ALTER TABLE `tbl_tag_post`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_tag` (`id_tag`), ADD UNIQUE KEY `id_post` (`id_post`);

--
-- Indexes for table `tbl_type`
--
ALTER TABLE `tbl_type`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_category_post`
--
ALTER TABLE `tbl_category_post`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `tbl_post`
--
ALTER TABLE `tbl_post`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_subscriber`
--
ALTER TABLE `tbl_subscriber`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_tags`
--
ALTER TABLE `tbl_tags`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_tag_post`
--
ALTER TABLE `tbl_tag_post`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_type`
--
ALTER TABLE `tbl_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
ADD CONSTRAINT `auth_group__permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
ADD CONSTRAINT `auth_group_permission_group_id_689710a9a73b7457_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
ADD CONSTRAINT `auth__content_type_id_508cf46651277a81_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
ADD CONSTRAINT `auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
ADD CONSTRAINT `auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
ADD CONSTRAINT `auth_user_u_permission_id_384b62483d7071f0_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
ADD CONSTRAINT `auth_user_user_permissi_user_id_7f0938558328534a_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
ADD CONSTRAINT `djang_content_type_id_697914295151027a_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
ADD CONSTRAINT `django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `tbl_category_post`
--
ALTER TABLE `tbl_category_post`
ADD CONSTRAINT `fk_id_category_id` FOREIGN KEY (`id_category`) REFERENCES `tbl_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_post_post_id` FOREIGN KEY (`id_post`) REFERENCES `tbl_post` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
ADD CONSTRAINT `fk_comment_post` FOREIGN KEY (`id_post`) REFERENCES `tbl_post` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_tag_post`
--
ALTER TABLE `tbl_tag_post`
ADD CONSTRAINT `fk_tag_post` FOREIGN KEY (`id_post`) REFERENCES `tbl_post` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_tag_tag_id` FOREIGN KEY (`id_tag`) REFERENCES `tbl_tags` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
