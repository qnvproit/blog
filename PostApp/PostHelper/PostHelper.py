import urllib2
import json
import requests

from django.http import HttpResponse
from django.db.models import Count

from PostApp.Models.Post import Post
from PostApp.Models.Category import Category
from PostApp.Models.PostCategory import PostCategory
from PostApp.Models.Comment import Comment
from PostApp.PostHelper.ConstHelper import ConstPost


def to_display_simple_list(post):
    """ Convert a Post object to list type
    """
    return {
        "id": post.id,
        "title": post.title,
        "content": post.content
    }
# End of to_display_simple_list


def to_display_custom_list(list_post, fields=[], is_tiny=False):
    """
    """
    result = []
    for post in list_post:
        result.append(post.to_display_json(fields, is_tiny=is_tiny))
    return result


def get_list_posts(offset, limit, search_by_title,
                   search_by_content, id_category):
    """ Get list post by params
    """

    result = Post.objects.all()
    if id_category is not None:
        result = result.filter(postvscategory__id_category=id_category)

    # Search by title, content
    result = result.filter(
        title__contains=search_by_title
    ).filter(
        content__contains=search_by_content
    )

    if (offset is not None) or (limit is not None):
        result = result[offset:(offset + limit)]

    return result
# End of get_list_post


def get_detail_post(id_post):
    """ Get detail post by id: title, content, ...
    """
    if id_post is None:
        return None
    return Post.objects.get(pk=id_post)
# End of get_detail_post


def increase_views(id_post):
    """
    """
    post = Post.objects.get(pk=id_post)
    post.number_views += 1
    post.save()
# End of increase_views

