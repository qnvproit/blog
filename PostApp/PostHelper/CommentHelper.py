import urllib2
import json
import requests

from django.http import HttpResponse
from django.db.models import Count

from PostApp.Models.Post import Post
from PostApp.Models.Category import Category
from PostApp.Models.PostCategory import PostCategory
from PostApp.Models.Comment import Comment


def get_list_comment(id_post):
    """ Get list Comments of post
    """

    if id_post is None:
        return None
    return Comment.objects.filter(active=1).filter(id_post=id_post)
# End of get_list_comment


def create_comment(content, id_post, id_parent_cmt,
                   name_owner, email_owner, is_notifications):
    """ Create new comment of a post
    """
    pass


def display_comments_to_json(comments, fields=[]):
    result = []
    for comment in comments:
        result.append(comment.to_display_json(fields=fields))
    return result


def feed_index_comment(comment):
    """ ElasticSearch index 
        Feed index of comment
    """
    if comment is None:
        return None

    data = '{"index": {"_id": %s}}\n' % comment.id
    data += json.dumps({
            'content': comment.content,
            'name_owner': comment.name_owner
        }) + "\n"

    response = request.put('http://127.0.0.1:9200/blog/comment/_bulk/', data=data)
    return response


def feed_index_all_comment():
    """ ElasticSearch
        Feed index of all comments (active)
    """
    list_comments = Comment.objects.filter(active=1)
    for comment in list_comments:
        feed_index_comment(comment)

