import urllib2
import json
import requests

from django.http import HttpResponse
from django.db.models import Count

from PostApp.Models.Post import Post
from PostApp.Models.Category import Category
from PostApp.Models.PostCategory import PostCategory
from PostApp.Models.Comment import Comment



def get_list_category():
    """
    """
    list_category = Category.objects.annotate(num_post=Count('categoryvscategory'))
    # for category in list_category:
    #     num = category.categoryvscategory_set.count()
    #     # category['']
    return list_category
