import json
import requests


def create_index_post():
    """
    """
    data = {
        "settings": {
            "number_of_shard": 5,
            "number_of_replicas": 1
        },
        "mappings": {
            "post": {
                "properties": {
                    "title": {"type": "string"},
                    "content": {"type": "string"}
                }
            }
        }
    }

    response = requests.put('http://127.0.0.1:9200/blog/', data=json.dumps(data))
    return response


def feed_index_post(post):
    """ ElasticSearch index:
        Feed index a post 
    """
    if post is None:
        return None
    data = '{"index": {"_id": %s}}\n' % post.id
    data += json.dumps({
                'title': post.title,
                'content': post.content,
        }) + "\n"
    
    response = requests.put('http://127.0.0.1:9200/blog/post/_bulk/', data=data)
    return response.text


def search_post(query_string):
    pass
