import requests

from django.http import HttpResponse


def check_token_capcha(token):
    """ Check token capcha by Google reCapcha API
    """
    data_request = {
        'secret': '6Le5QQoTAAAAAPmwMlG6N3uxw52OD-U9KnNtocOm',
        'response': token
    }
    response = requests.post('https://www.google.com/recaptcha/api/siteverify', 
                            params=data_request)
    return HttpResponse(response.json()['success'])