import urllib2
import json
import requests
import time
import math

from django.shortcuts import render_to_response
from django.template.loader import *
from django.http import HttpResponse
from django.http import JsonResponse
from django.http import Http404
from django.db.models import Count
from django.core.mail import send_mail
from django.core import serializers
from django.core.cache import cache

from Helper import AjaxHelper
from PostApp.Models.Post import Post
from PostApp.Models.Comment import Comment
from PostApp.Models.Category import Category
from PostApp.Models.PostCategory import PostCategory
from PostApp.Models.Type import Type
from PostApp.PostHelper import PostHelper
from PostApp.PostHelper import CommentHelper
from PostApp.PostHelper import CategoryHelper
from PostApp.PostHelper import CapchaHelper
from PostApp.PostHelper.SearchHelper import PostSearchHelper


# Create your views here.
def test_views(request):
    """ Just for testing
    """
    try:
        # posts = Post.objects.exclude(pk=1)
        # for item in posts:
        #     PostHelper.feed_index_post(item)
        
        data = {
            "fields": ["_id"],
            "from": 0,
            "size": 10,
            "query": {
                # "term": {
                #     "content": "tutorial"
                # }
                "filtered": {
                    "query": {
                        "query_string": {
                            "query": "have"
                        }
                    }
                }
            }
        }        

        response = requests.post('http://127.0.0.1:9200/blog/post/_search', data=json.dumps(data))

       

        context = {
            "test_item": response.text
        }
        return render_to_response("test.html", context)

    except Exception, e:
        return AjaxHelper.ajax_error(str(e))


def detail_post(request, id_post):
    """ Show detail about post
    """
    try:
        if id_post is None:
            return None

        # location = request.get_full_path()
        # return HttpResponse(request.build_absolute_uri(location))

        # Get detail
        post = PostHelper.get_detail_post(id_post)

        # Increase number views
        PostHelper.increase_views(id_post)

        # Get Comments
        comments = CommentHelper.get_list_comment(id_post)

        # Get category (tags)
        tags = Category.objects.filter(categoryvscategory__id_post=id_post)

        # Get list newest post (for slider in footer)
        list_new_post = Post.objects.order_by('-id')[0:10]

        # Get list_category
        list_category = cache.get('list_category')
        if list_category is None:
            list_category = Category.objects.all().annotate(num_posts=Count('categoryvscategory'))
            cache.set('list_category', list_category, 500)

        # Get list top view post
        list_top_view_post = cache.get('list_top_view_post')
        if list_top_view_post is None:
            list_top_view_post = Post.objects.order_by('number_views')[0:5]
        else:
            list_top_view_post = cache.get('list_top_view_post')

        if post is None:
            raise Http404("This post does not exist.")
        else:
            context = {
                "post": post,
                "comments": comments,
                'tags': tags,
                'list_new_post': list_new_post,
                'list_category': list_category,
                'list_top_view_post': list_top_view_post
            }
            return render_to_response("detailpost.html", context)

    except Post.DoesNotExist, e:
        raise Http404("Nothing : " + str(e))

    except Exception, e:
        raise e
# End of detail_post


def get_post_by_tag(request, id_category, page):
    """
    """
    try:
        offset_start = (int(page) - 1) * 7

        # Get list posts
        list_posts = Post.objects.filter(postvscategory__id_category=id_category)[offset_start:(offset_start+7)]

        # Get number posts
        num_posts = Post.objects.filter(postvscategory__id_category=id_category).count()

        num_pages = int(math.ceil(num_posts / 2.0))

        # Get category 
        category = Category.objects.get(pk=id_category)

        # Get list newest post (for slider in footer)
        list_new_post = Post.objects.order_by('-id')[0:10]

        # Get list_category
        list_category = cache.get('list_category')
        if list_category is None:
            list_category = Category.objects.all().annotate(num_posts=Count('categoryvscategory'))
            cache.set('list_category', list_category, 500)

        # Get list top view post
        list_top_view_post = cache.get('list_top_view_post')
        if list_top_view_post is None:
            list_top_view_post = Post.objects.order_by('number_views')[0:5]
        else:
            list_top_view_post = cache.get('list_top_view_post')

        context = {
            'number_post': num_posts,
            'list_post': list_posts,
            'num_pages': range(num_pages),
            'current_page': int(page),
            'id_category': id_category,
            'list_category': list_category,
            'current_category': category,
            'list_new_post': list_new_post,
            'list_top_view_post': list_top_view_post
        }
        return render_to_response("category.html", context)

    except Exception, e:
        raise e
# End of get_post_by_type


def ajax_get_list_post_by_tag(request):
    """
    """
    try:
        params = request.REQUEST
        
        # sort_by = if params.get('sort_by') is None: 'id' else: params.get('sort_by')
        sort_by = 'id' if params.get('sort_by') else params.get('sort_by')

        # page = if params.get('page') is None: 1 else: int(params.get('page'))
        page = 1 if params.get('page') is None else int(params.get('page'))
        
        id_category = 1 if params.get('id_category') is None else int(params.get('id_category'))

        offset_start = (page - 1) * 7

        list_post = Post.objects.filter(postvscategory__id_category=id_category).order_by(sort_by)[offset_start:(offset_start+7)]

        return AjaxHelper.ajax_ok(
                            PostHelper.to_display_custom_list(list_post, is_tiny=True),
                            "Success")

    except Exception, e:
        return AjaxHelper.ajax_error(str(e))
# End of ajax_get_list_post_by_tag


def get_post_by_type(request, id_type, page):
    """
    """
    try:
        page = 1 if page is None else int(page)

        id_type = 1 if id_type is None else int(id_type)

        offset_start = (page - 1) * 7

        # Get list posts
        list_post = Post.objects.filter(type_post=id_type)[offset_start:(offset_start+7)].select_related()

        # Get number posts
        num_post = Post.objects.filter(type_post=id_type).count()

        # Get Type post 
        type_post = Type.objects.select_related().get(pk=id_type)

        # Get list newest post (for slider in footer)
        list_new_post = Post.objects.order_by('-id')[0:10]

        # Get list_category
        list_category = cache.get('list_category')
        if list_category is None:
            list_category = Category.objects.all().annotate(num_posts=Count('categoryvscategory'))
            cache.set('list_category', list_category, 500)

        # Get list top view post
        list_top_view_post = cache.get('list_top_view_post')
        if list_top_view_post is None:
            list_top_view_post = Post.objects.order_by('number_views')[0:5]
        else:
            list_top_view_post = cache.get('list_top_view_post')


        context = {
            'list_post': list_post,
            'num_post': num_post,
            'page': page,
            'num_pages': math.ceil(num_post / 7.0),
            'type_post': type_post,
            'list_category': list_category,
            'list_top_view_post': list_top_view_post,
            'list_new_post': list_new_post
        }

        return render_to_response('type_post.html', context)

    except Exception, e:
        raise e
# End of get_post_by_type


def create_new_comment(request):
    """
    """
    try:
        params = request.REQUEST

        # Check capcha bot
        if CapchaHelper.check_token_capcha(params.get("token_capcha")) is False:
            return AjaxHelper.ajax_error("Please re-check capcha !")

        post = Post.objects.get(pk=params.get('id_post'))

        new_cmt = Comment(content=params.get('content'),
                          id_parent_cmt=int(params.get('id_comment_parent')),
                          id_post=post,
                          created_time=int(time.time()),
                          name_owner=params.get('name'),
                          email_owner=params.get('email'),
                          active=1,
                          is_notifications=params.get('notify'))
        new_cmt.save()

        # Get list comments
        list_comments = post.comment_set.all()
        return AjaxHelper.ajax_ok(
                        CommentHelper.display_comments_to_json(
                                                    list_comments,
                                                    fields=[
                                                        'id', 'content', 'name_owner', 
                                                        'email_owner', 'created_time', 
                                                        'id_parent_cmt'
                                                    ]
                        ), "Success")

    except Post.DoesNotExist, e:
        return AjaxHelper.ajax_error("This post doesnot exist.")
    except Exception, e:
        return AjaxHelper.ajax_error("Error: " + str(e))
# End of new_comment


def ajax_validate_token(request):
    """
    """
    try:
        params = request.REQUEST
        token = params.get('token')

        # Validate
        data_request = {
            'access_token': token
        }
        response = requests.get('https://www.googleapis.com/oauth2/v1/tokeninfo', data_request)
        # if getattr(response, 'error') is None:
        #     # Invalid Token
        #     return AjaxHelper.ajax_error('Access Token is invalid.')

        # Call Google API to get information user
        response = requests.get('https://www.googleapis.com/auth/userinfo.profile', data_request)

        return HttpResponse(response)
        return AjaxHelper.ajax_ok(response.email, 'Success')

    except Exception, e:
        return AjaxHelper.ajax_error(str(e))



def send(request):
    """
    """
    # get content Html
    t = get_template("mail/test.html")  # t is a Template object

    m = t.render(Context({"name": "Quy"}))   # m is a string (SafeText)

    num_sucess = send_mail("Subject here",
                           "Content mail here",
                           'nvqbkhn@gmail.com',
                           ['qnvproit@gmail.com'],
                           fail_silently=False,
                           html_message=m
                           )

    return HttpResponse(num_sucess)
# End of send


def ajax_search(request):
    """
    """
    params = request.REQUEST
    query_string = params.get('query')

    if query_string == '':
        return AjaxHelper.ajax_ok(None,'')

    # Search post 
    # response = 



def test_capcha(request):
    """
    """
    try:

        data_request = {
            'secret': '6Le5QQoTAAAAAPmwMlG6N3uxw52OD-U9KnNtocOm',
            'response': '03AHJ_Vuub9w2KEz9B8-oiS7f7fMxnESNipwTXr1UjgbU-a8IAwNejnUh-x3d5tmccApSsUnX6DJ6NPNzdKxvNbvEX6SFqSTU1zXIJCm3hYxtMV6p6sPANk0kLTs4-UX59qznwBEE1SOzvP_1JRJaVVwHBfkGI9C11Wi4bXkZPjX7ZVZx0SgwhG10y9mr9GTaCuTCG3zT1RgAbWlUt23IcqDJI7bKcygWtG-db_f7tG3WFMko5GFBUIozbxnreb-DaY1xA4tkkSxOyNbbrEghn1QJcUKlV8tmNZhvKXDXaOn3afkdS0tot6SvG0j3acoaqG_nRUlNNJdxG91qTG6ZI5cZGS4nwcgVs0jwQeeWx8E3XK3g9qSULhp4megLGYNTCAKHj9X8vCN8kMBVuN-zehFNFndY9J5t3TjysibhxIRZiawanGOkP6FQ'
        }
        response = requests.post('https://www.google.com/recaptcha/api/siteverify',
                                 params=data_request)
        return HttpResponse(response.json()['success'])

    except Exception, e:
        raise e
