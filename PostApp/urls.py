from django.conf.urls import url
import views

urlpatterns = [
    url(r'^test/$', views.test_views),
    url(r'^detail/(?P<id_post>(\d)*)/$', views.detail_post),
    url(r'^category/(?P<id_category>(\d)*)/(?P<page>(\d)*)$', views.get_post_by_tag),
    url(r'^ajax_list_posts/$', views.ajax_get_list_post_by_tag),
    url(r'^type/(?P<id_type>(\d)*)/(?P<page>(\d)*)$', views.get_post_by_type),
    url(r'^ajax_validate_token/$', views.ajax_validate_token),
    url(r'^send/$', views.send),
    url(r'^new_comment/$', views.create_new_comment),
    url(r'^capcha/$', views.test_capcha)
]
