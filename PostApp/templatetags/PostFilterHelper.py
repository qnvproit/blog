from django import template

from PostApp.PostHelper.Const.ConstHelper import ConstPost

register = template.Library()


@register.filter(name="avatar")
def get_full_url_avatar(avatar):
    return (ConstPost.PREFIX_AVATAR_POST + 'noavatar.png') if (avatar is None or avatar == '') else (ConstPost.PREFIX_AVATAR_POST + avatar)
