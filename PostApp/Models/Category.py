from django.db import models


class Category(models.Model):
    title = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'tbl_category'
        app_label = 'PostApp'
