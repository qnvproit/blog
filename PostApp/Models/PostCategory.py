from django.db import models

from PostApp.Models.Post import Post
from PostApp.Models.Category import Category


class PostCategory(models.Model):
    id_post = models.ForeignKey(Post, db_column='id_post',
                                related_name='postvscategory')
    id_category = models.ForeignKey(Category, db_column='id_category',
                                    related_name="categoryvscategory")

    class Meta:
        managed = False
        db_table = 'tbl_category_post'
        app_label = 'PostApp'
