from django.db import models


class Type(models.Model):
    type_post = models.CharField(max_length=250)
    active = models.IntegerField(default=1)

    class Meta:
        managed = False
        db_table = 'tbl_type'
        app_label = 'PostApp'
