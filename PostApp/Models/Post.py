from django.db import models

from PostApp.PostHelper.ConstHelper import ConstPost
from PostApp.Models.Type import Type


class Post(models.Model):
    title = models.CharField(max_length=250)
    content = models.TextField()
    created_time = models.BigIntegerField()
    active = models.IntegerField()
    type_post = models.ForeignKey(Type, db_column='type_post', 
                                    related_name='post_type')
    number_views = models.IntegerField(default=0)
    avatar = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'tbl_post'
        app_label = 'PostApp'

    def __str__(self):
        return self.title

    def to_display_json(self, fields=[], is_tiny=False):
        if fields == []:
            return {
                'id': self.id,
                'title': self.title,
                'content': (self.content[:150] + " ...") if is_tiny else self.content,
                'created_time': self.created_time,
                'number_views': self.number_views,
                'avatar': PostHelper.get_full_url_avatar(self.avatar),
            }
        else:
            result = {}
            for field in fields:
                result[field] = getattr(self, field)
            if is_tiny:
                result['content'] = (self.content[:150] + " ...")
            resutl['avatar'] = PostHelper.get_full_url_avatar(result['avatar'])
            return result
