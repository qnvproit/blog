from django.db import models

from PostApp.Models.Post import Post


class Comment(models.Model):
    content = models.TextField()
    id_post = models.ForeignKey(Post, db_column='id_post',null=True)
    id_parent_cmt = models.IntegerField()
    created_time = models.BigIntegerField()
    name_owner = models.CharField(max_length=250)
    email_owner = models.CharField(max_length=250)
    active = models.IntegerField()
    is_notifications = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'tbl_comment'
        app_label = 'PostApp'

    def __str__(self):
        return self.content

    def __unicode__(self):
        return self.content

    def to_display_json(self, fields=[]):
        if fields == []:
            return {
                "id": self.id,
                "content": self.content,
                "id_post": self.id_post.id,
                "id_parent_cmt": self.id_parent_cmt,
                "created_time": self.created_time,
                "name_owner": self.name_owner,
                "email_owner": self.email_owner
            }
        else:
            result = {}
            for field in fields:
                result[field] = getattr(self, field)
            return result