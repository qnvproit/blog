# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=250)),
            ],
            options={
                'db_table': 'tbl_category',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.TextField()),
                ('id_parent_cmt', models.IntegerField()),
                ('created_time', models.BigIntegerField()),
                ('name_owner', models.CharField(max_length=250)),
                ('email_owner', models.CharField(max_length=250)),
                ('active', models.IntegerField()),
                ('is_notifications', models.IntegerField()),
            ],
            options={
                'db_table': 'tbl_comment',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=250)),
                ('content', models.TextField()),
                ('created_time', models.BigIntegerField()),
                ('active', models.IntegerField()),
                ('number_views', models.IntegerField(default=0)),
                ('avatar', models.CharField(max_length=250)),
            ],
            options={
                'db_table': 'tbl_post',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PostCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'tbl_category_post',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Type',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type_post', models.CharField(max_length=250)),
                ('active', models.IntegerField(default=1)),
            ],
            options={
                'db_table': 'tbl_type',
            },
        ),
    ]
