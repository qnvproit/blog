/* For page typepost.html */

function get_list_posts_by_category(page) {
    sort_by = $('#sort-by').val();

    $.ajax({
        url: base_url + '/post/ajax_list_posts/',
        type: 'POST',
        data: {
            sort_by: 'id',
            page: page,
            id_category: $('#id-category').val(),
            csrfmiddlewaretoken: getCookie("csrftoken")
        },
        success: function(data) {
            console.log(data);
            console.log(data.data);

            list_post = data.data;
            html = "";
            if(list_post.length != 0) {
                for( i=0; i<list_post.length; i++) {
                    post = list_post[i];
                    temp = "<div class='card light-green lighten-1 waves-effect waves-light item-post'>";
                    temp += "<a href='/post/detail/" + post.id + "'>";
                    temp += "<div class='card-content'>";
                    temp += "<span class='card-title'>" + post.title + "</span>";
                    temp += "<p>";
                    temp += post.content;
                    temp += "</p>";
                    temp += "<div>";
                    temp += "<span class='pull-right'>"
                    temp += convert_timestamp_to_date(post.created_time);
                    temp += "</span>";
                    temp += "</div>"
                    temp += "</div>";
                    temp += "</a>";
                    temp += "</div>";

                    html += temp;
                } 
            } else {
                html = nothing_html;
            }
            

            $('#list_post').html(html).hide().slideDown(300);
            $('html, body').scrollTop($('#card-panel-1').offset().top - 100);
            $('#current-page').val(page);
            $('.pagination li').removeClass('active');
            $('#btn-page-' + page).addClass('active');
        },
        error: function(data) {
            console.log(data)
            $('#list_post').html(html_content_error(data.message));
        }
    })
}


function go_to_next_page_post() {
    current_page = $('#current-page').val();
    max_page = $('#max-page').val();
    if( current_page >= max_page ) {
        return;
    }
    get_list_posts_by_category(++current_page);
}


function go_to_prev_page_post() {
    current_page = $('#current-page').val();
    console.log( current_page );
    if( current_page <= 1 ) {
        return;
    }
    get_list_posts_by_category(--current_page);
}