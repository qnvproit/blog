var base_url = "http://127.0.0.1:8000";

var nothing_html = "<div class='card grey lighten-4 waves-effect waves-light item-post'>" + "<div class='card-content text-center bold italic'><span class='glyphicon glyphicon-folder-close'></span>&nbsp;&nbsp;NOTHING HERE" + "</div>" + "</div>";


function html_content_error(message) {
    html = "<div class='card red lighten-2 waves-effect waves-light item-post'>";
    html += "<div class='card-content text-center bold italic'>";
    html += message;
    html += "</div>";
    html += "</div>";

    return html;
}

function valid_email( email ) {
    rex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

    return  (email.length != 0) && rex.test(email);
}


function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


function convert_timestamp_to_date(timestamp) {
    var date = new Date(timestamp*1000);
    year = date.getFullYear();
    temp_month = date.getMonth() + 1;
    month = (temp_month < 10)?('0'+temp_month):temp_month;
    
    temp_day = date.getDate();
    day = (temp_day < 10)?('0'+temp_day):(temp_day);

    temp_hour = date.getHours();
    hours = (temp_hour < 10)?('0'+temp_hour):temp_hour;

    temp_minutes = date.getMinutes();
    minutes = (temp_minutes < 10)?('0'+temp_minutes):temp_minutes;


    return year + "-" + month + "-" + day + " " + temp_hour + ":" + temp_minutes;
}

function go_top() {
    $('html, body').animate({
        'scrollTop': 0
    }, 500);
}


function add_new_subscriber() {
    email = $('#email-subscriber').val().trim();
    if(!valid_email(email)) {
        Materialize.toast("Invalid Email", 3000);
        return;
    }
    $.ajax({
        url: base_url + '/subscriber/add/',
        type: 'POST',
        data: {
            'email': email,
            csrfmiddlewaretoken: getCookie("csrftoken")
        },
        success: function(data) {
            console.log(data);
            Materialize.toast(data.message, 3000);
            $('#email-subscriber').val('');
        },
        error: function(data) {
            Materialize.toast("Something went wrong ! Please try again later." , 3000);
            $('#email-subscriber').val('');
        }

    })
}