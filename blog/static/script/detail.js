
function create_new_comment() {
	name = $('#username').val();
	email = $('#email').val();
	content = $('#content-cmt').val();
	notify = $('#check-notify').is(":checked");

    if( name.length == 0 ) {
        $('#content-error').html("Please fill your name !");
        return;
    }

    // if( valid_email( email ) == false ) {
    //     alert( valid_email( email ) );
    //     $('#content-error').html("Please provide valid email");
    //     return;
    // }

    if( content.length < 10 ) {
        $('#content-error').html("Comment must be more than 10 characters long !");
        return;
    }

    token_capcha = grecaptcha.getResponse();
    if( token_capcha.length == 0 ) {
        $('#content-error').html("Please check capcha !");
        return;
    }
    
    id_comment_parent = ($('#id_comment_parent').val() == '')?0:$('#id_comment_parent').val();

	$.ajax({ 
		url: base_url + "/post/new_comment/",
		type: "POST",
		data: {
			name: name,
			email: email,
			content: content,
            id_post: $('#current_id_post').val(),
            id_comment_parent: id_comment_parent,
			notify: (notify)?1:0,
            token_capcha: token_capcha,
            csrfmiddlewaretoken: getCookie("csrftoken")
		}, 
		success: function( data ) {
			console.log( data );
            add_list_comments(data.data);
            reset_editor_comment();
            $('html body').animate({
                'scrollTop': $('#start-comments').offset().top - 100
            }, 500);
            // $('html body').scrollTop( $('#start-comments').offset().top - 100 );
		},
		error: function( data ) {
			console.log( data );
		}	
	})

}


function my_test(list) {
    return 0;
}

function add_list_comments(list_comments) {
    html = "";
    listChildCmt = [];
    for(i=1; i<=list_comments.length; i++) {
        comment = list_comments[i-1];
        if( comment.id_parent_cmt != 0 ) {
            listChildCmt.push( comment );
            continue;
        } 

        temp = "<div id='cmt-" + comment.id + "' class='card-panel clearfix teal accent-1 container-cmt'>";
        temp += "<div>";
        temp += "<img src='/static/media/image/user.png'" + " class='img img-thumbnail pull-left user-profile' width='60px'>";
        temp += "<div class='content-comment'>";
        temp += "<span class='name_owner'>" + comment.name_owner + "</span><br/>";
        temp += comment.content;
        temp += "<div class='utils-cmt'>";
        temp += "<i class='material-icons tooltipped' data-position='left' data-delay='50' data-tooltip='Reply this comment'>comment</i>";

        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";

        temp += "<div class='div-sub-cmt' id='div-sub-cmt-" + comment.id + "'>";

        temp += "</div>";

        html += temp;
    }

    $('#card-panel-3').html( html );

}


function organize_child_cmt( ) {
    $('#card-panel-3 .sub-container-cmt').each( function( ) {
        console.log('fuck ');
        id = $(this).attr('id');
        html = $(this).html();
        $('#cmt-' + id + ' .div-sub-cmt').append( html );
        $(this).hide();
    })
}


function addChildComment( listChildCmt ) {
    for( i=0; i<listChildCmt.length; i++ ) {
        temp = "<div id='sub-cmt-" + comment.id + "' class='card-panel clearfix teal accent-1 child-container-cmt'>";
        temp += "<div>";
        temp += "<img src='/static/media/image/user.png'" + " class='img img-thumbnail pull-left' width='60px'>";
        temp += "<div class='content-comment'>";
        temp += "<span class='name_owner'>" + comment.name_owner + "</span><br/>";
        temp += comment.content;
        // temp += "<div class='utils-cmt'>";
        // temp += "<i class='material-icons tooltipped' data-position='left' data-delay='50' data-tooltip='Reply this comment'>comment</i>";

        // temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";

        $('#div-sub-cmt-' + comment.id).append( temp );
    }
}   


function reset_editor_comment() {
    $('#username').val('');
    $('#email').val('');
    $('#content-cmt').val('');
    $('#check-notify').attr('checked', false);
    $('#more-submit-cmt').slideDown();
}

function prepare_add_comment() {
    $('html body').animate({
        'scrollTop': $('#editor-comment').offset().top - 100
    }, 500);
    $('#username').focus();
    $('#more-submit-cmt').slideDown();
}

function showMoreSubmitCmt() {
    $('#more-submit-cmt').slideDown();
}

function hideMoreSubmitCmt() {
    $('#more-submit-cmt').hide();
}


function show_token_capcha() {
    console.log( grecaptcha.getResponse() )
}