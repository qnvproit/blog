import requests


from django.shortcuts import render_to_response
from django.template.loader import *
from django.http import HttpResponse
from django.http import JsonResponse
from django.http import Http404
from django.db import IntegrityError

from Helper import AjaxHelper
from SubscriberApp.Models.Subscriber import Subscriber

# Create your views here.


def add_subscriber(request):
    """ Add new subscriber (email)
    """
    try:
        params = request.REQUEST

        email = params.get('email')

        Subscriber.objects.create(email=email)

        return AjaxHelper.ajax_ok(None, "Successfully")

    except IntegrityError, e:
        return AjaxHelper.ajax_error('This email already exists in system. Please use other email.')

    except Exception, e:
        return AjaxHelper.ajax_error(str(e))
