from django.db import models


class Subscriber(models.Model):
    email = models.CharField(max_length=50, unique=True)
    active = models.IntegerField(default=1)

    class Meta:
        db_table = 'tbl_subscriber'
        app_label = 'PostApp'
